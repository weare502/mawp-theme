/* eslint no-inner-declarations: 0, no-unused-vars: 0 */
(function($) {
    $(document).ready(function() {
		let $body = $('body'); // function scoped body class

		// mobile menu open/close functions
		$(function siteNavigation() {
			$('#menu-toggle').click(function() {
				$('.x-bar').toggleClass('x-bar-active');
				$('#primary-menu').slideToggle(550);

				// keeps structure and fades the rest of the header out
				$('.header-container').toggleClass('content-fade');
			});

			// make sure we are on mobile since we are targeting classes
			if( $body.width() < 769 ) {
				$('.menu-item-has-children').click(function() {
					var $sub = $(this).find('.sub-menu:first');
					$sub.slideToggle(550);
				});
			}
		});

		// white bar at bottom of header (below page title)
		$(function whiteBarControl() {
			if( $body.hasClass('single-project') || $body.hasClass('home') ) {
				$('.white-bar').css('display', 'none');
			}
		});

		// dropdown block controls and aria events for screenreaders
		$(function dropDownBlock() {
			$('.dropdown-content').each(function() {
				$(this).hide();
			});

			$('.dropdown-block-title').click(function() {
				// fires on first click (when content is expanded)
				if( $(this).hasClass('set-border') ) {
					$(this).removeClass('set-border');
					$(this).next('.dropdown-content').slideToggle(700);
					$(this).attr('aria-pressed', 'true');
				} else {
					// fires on second click (when content is closed)
					$(this).next('.dropdown-content:first').slideToggle(700, function(){
						$(this).prev('.dropdown-block-title').addClass('set-border');
						$(this).prev('.dropdown-block-title').attr('aria-pressed', 'false');
					});
				}
				// always fire
				$(this).toggleClass('chevron-rotate');
			});
		});

		// single project gallery - per project
		if( $body.hasClass('single-project') ) {
			$(function gallerySlider() {
				var $slick_gallery = $('.project-gallery');

				$slick_gallery.slick({
					slidesToShow: 1,
					slidesToScroll: 1,
					autoplay: false,
					accessibility: true,
					draggable: true,
					dots: false,
					arrows: true,
					appendArrows: $('.gallery-arrows'),
					prevArrow: $('.prev-arrow'),
					nextArrow: $('.next-arrow'),
					responsive: [{
						breakpoint: 1200,
						settings: {
							appendArrows: null
						}
					}]
				});
			});
		}

		// slider setup - only run on homepage
		if( $body.hasClass('home') ) {
			$(function slideLoader() {
				var $slick = $('.slider-wrapper');

				// Featured Project Slider
				$slick.slick({
					slidesToShow: 1,
					slidesToScroll: 1,
					autoplay: true,
					autoplaySpeed: 5000,
					pauseOnHover: false,
					dots: true,
					appendDots: $('.slider-dots'),
					accessibility: true,
					arrows: false,
					draggable: false
				});
			});
		} // end of home body class check

		// Block and part of Home Page template
		$(function infiniteCarousel() {
			var $slick_logo = $('.carousel');

			// Infinite Logo Carousel
			$slick_logo.slick({
				slidesToScroll: 1,
				autoplay: true,
				autoplaySpeed: 3000,
				infinite: true,
				variableWidth: true,
				centerMode: true,
				dots: false,
				arrows: false,
				accessibility: true,
				draggable: true,
				responsive: [{
					breakpoint: 480,
					settings: {
						slidesToShow: 3
					}
				}]
			});
		});

		// checks for the url on page load when coming from home page tax links or menu tax links
		$(function projectTaxLink() {
			var url = window.location.href;

			if( url.indexOf('commercial') > -1 ) {
				$('div.category-intro#commercial').toggle();
				$('div.feature-block#commercial').toggle();
			} else if( url.indexOf('education') > -1 ) {
				$('div.category-intro#education').toggle();
				$('div.feature-block#education').toggle();
			} else if( url.indexOf('gov-and-military') > -1 ) {
				$('div.category-intro#gov-and-military').toggle();
				$('div.feature-block#gov-and-military').toggle();
			} else if( url.indexOf('healthcare') > -1 ) {
				$('div.category-intro#healthcare').toggle();
				$('div.feature-block#healthcare').toggle();
			} else if( url.indexOf('residential') > -1 ) {
				$('div.category-intro#residential').toggle();
				$('div.feature-block#residential').toggle();
			}

		});
	}); // end Document.Ready

	$(document).on('click', 'div.facetwp-radio', function() {
		var $this = $(this);
		var btn_val = $this.attr('data-value');

		// toggle() instead of show() allows the value to be hidden if the same button is clicked (unselected)
		// I am sorry
		if( btn_val === 'commercial' ) {
			$('div.category-intro#commercial').toggle();
			$('div.feature-block#commercial').toggle();

			$('div.category-intro#education').hide();
			$('div.feature-block#education').hide();

			$('div.category-intro#gov-and-military').hide();
			$('div.feature-block#gov-and-military').hide();

			$('div.category-intro#healthcare').hide();
			$('div.feature-block#healthcare').hide();

			$('div.category-intro#residential').hide();
			$('div.feature-block#residential').hide();

		} else if( btn_val === 'education') {
			$('div.category-intro#commercial').hide();
			$('div.feature-block#commercial').hide();

			$('div.category-intro#education').toggle();
			$('div.feature-block#education').toggle();

			$('div.category-intro#gov-and-military').hide();
			$('div.feature-block#gov-and-military').hide();

			$('div.category-intro#healthcare').hide();
			$('div.feature-block#healthcare').hide();

			$('div.category-intro#residential').hide();
			$('div.feature-block#residential').hide();

		} else if( btn_val === 'gov-and-military' ) {
			$('div.category-intro#commercial').hide();
			$('div.feature-block#commercial').hide();

			$('div.category-intro#education').hide();
			$('div.feature-block#education').hide();

			$('div.category-intro#gov-and-military').toggle();
			$('div.feature-block#gov-and-military').toggle();

			$('div.category-intro#healthcare').hide();
			$('div.feature-block#healthcare').hide();

			$('div.category-intro#residential').hide();
			$('div.feature-block#residential').hide();

		} else if( btn_val === 'healthcare' ) {
			$('div.category-intro#commercial').hide();
			$('div.feature-block#commercial').hide();

			$('div.category-intro#education').hide();
			$('div.feature-block#education').hide();

			$('div.category-intro#gov-and-military').hide();
			$('div.feature-block#gov-and-military').hide();

			$('div.category-intro#healthcare').toggle();
			$('div.feature-block#healthcare').toggle();

			$('div.category-intro#residential').hide();
			$('div.feature-block#residential').hide();

		} else if ( btn_val === 'residential' ) {
			$('div.category-intro#commercial').hide();
			$('div.feature-block#commercial').hide();

			$('div.category-intro#education').hide();
			$('div.feature-block#education').hide();

			$('div.category-intro#gov-and-military').hide();
			$('div.feature-block#gov-and-military').hide();

			$('div.category-intro#healthcare').hide();
			$('div.feature-block#healthcare').hide();

			$('div.category-intro#residential').toggle();
			$('div.feature-block#residential').toggle();
		}
	}); // end of document click event
})(jQuery);