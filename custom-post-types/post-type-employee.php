<?php

$labels = array(
	'name'               => __( 'Employees', 'mawp' ),
	'singular_name'      => __( 'Employee', 'mawp' ),
	'add_new'            => _x( 'Add Employee', 'mawp', 'mawp' ),
	'add_new_item'       => __( 'Add Employee', 'mawp' ),
	'edit_item'          => __( 'Edit Employee', 'mawp' ),
	'new_item'           => __( 'New Employee', 'mawp' ),
	'view_item'          => __( 'View Employee', 'mawp' ),
	'search_items'       => __( 'Search Employees', 'mawp' ),
	'not_found'          => __( 'No Employees found', 'mawp' ),
	'not_found_in_trash' => __( 'No Employees found in Trash', 'mawp' ),
	'parent_item_colon'  => __( 'Parent Employee:', 'mawp' ),
	'menu_name'          => __( 'Employees', 'mawp' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-businessman',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array( 'editor', 'title' ),
);
register_post_type( 'employee', $args );