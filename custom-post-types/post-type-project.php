<?php

$labels = array(
	'name'               => __( 'Projects', 'mawp' ),
	'singular_name'      => __( 'Project', 'mawp' ),
	'add_new'            => _x( 'Add Project', 'mawp', 'mawp' ),
	'add_new_item'       => __( 'Add Project', 'mawp' ),
	'edit_item'          => __( 'Edit Project', 'mawp' ),
	'new_item'           => __( 'New Project', 'mawp' ),
	'view_item'          => __( 'View Project', 'mawp' ),
	'search_items'       => __( 'Search Projects', 'mawp' ),
	'not_found'          => __( 'No Projects found', 'mawp' ),
	'not_found_in_trash' => __( 'No Projects found in Trash', 'mawp' ),
	'parent_item_colon'  => __( 'Parent Project:', 'mawp' ),
	'menu_name'          => __( 'Projects', 'mawp' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array('project-type'),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-hammer',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true, // main project listing
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array( 'editor', 'title', 'thumbnail', 'excerpt' ),
);
register_post_type( 'project', $args );