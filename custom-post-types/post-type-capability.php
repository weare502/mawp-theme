<?php

$labels = array(
	'name'               => __( 'Capabilities', 'mawp' ),
	'singular_name'      => __( 'Capability', 'mawp' ),
	'add_new'            => _x( 'Add Capability', 'mawp', 'mawp' ),
	'add_new_item'       => __( 'Add Capability', 'mawp' ),
	'edit_item'          => __( 'Edit Capability', 'mawp' ),
	'new_item'           => __( 'New Capability', 'mawp' ),
	'view_item'          => __( 'View Capability', 'mawp' ),
	'search_items'       => __( 'Search Capabilities', 'mawp' ),
	'not_found'          => __( 'No Capabilities found', 'mawp' ),
	'not_found_in_trash' => __( 'No Capabilities found in Trash', 'mawp' ),
	'parent_item_colon'  => __( 'Parent Capability:', 'mawp' ),
	'menu_name'          => __( 'Capabilities', 'mawp' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-rest-api',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true, // main capability page
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array( 'editor', 'title', 'excerpt' ),
);
register_post_type( 'capability', $args );