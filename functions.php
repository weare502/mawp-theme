<?php

// change 'views' directory to 'templates'
Timber::$locations = __DIR__ . '/templates';

class MAWPSite extends TimberSite {

	function __construct() {
		// Action Hooks //
		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'enqueue_block_assets', array( $this, 'backend_frontend_styles' ) );
		add_action( 'admin_head', array( $this, 'admin_head_css' ) );
		add_action( 'admin_menu', [ $this, 'admin_menu_cleanup'] );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'project_taxonomy' ) );
		add_action( 'acf/init', array( $this, 'render_custom_acf_blocks' ) );

		// Filter Hooks //
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'allowed_block_types', array( $this, 'custom_block_picker' ) );
		add_filter( 'gform_enable_field_label_visibility_settings', array( $this, '__return_true' ) );
		add_filter( 'block_categories', array( $this, 'mawp_block_category' ), 10, 2 );

		// Column Removal for Comments //
		add_filter( 'manage_edit-page_columns', array( $this, 'disable_admin_columns' ) );

		parent::__construct();
	}

	// hide admin area annoyances
	function admin_head_css() {
		?><style type="text/css">
			div.components-notice-list { display: none !important; }
			#wp-admin-bar-comments { display: none !important; }
			.update-nag { display: none !important; }
			#menu-posts-project { margin-top: 1rem !important; }
			#menu-posts-capability { margin-bottom: 0.5rem !important; }
		</style><?php
	}

	// remove unused / unecessary things
	function admin_menu_cleanup() {
		remove_menu_page( 'edit.php' ); // Posts
		remove_menu_page( 'edit-comments.php' ); // Comments
	}

	function enqueue_scripts() {
		$version = '20000022';
		wp_enqueue_style( 'mawp-css', get_stylesheet_directory_uri() . '/style.css', array(), $version );
		wp_enqueue_script( 'mawp-js', get_template_directory_uri() . '/static/js/site-dist.js', array( 'mawp-slider-js' ), $version );
		wp_enqueue_script( 'mawp-slider-js', get_template_directory_uri() . '/static/js/slick.min.js', array( 'jquery' ), $version );
	}

	// Uses the 'enqueue_block_assets' hook
	function backend_frontend_styles() {
		wp_enqueue_style( 'blocks-css', get_stylesheet_directory_uri() . '/block-style.css' );
	}

	// Custom Timber context helper functions
	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['date'] = date('F j, Y');
		$context['date_year'] = date('Y');
		$context['options'] = get_fields('option');
		$context['is_home'] = is_home();
		$context['home_url'] = home_url('/');
		$context['is_front_page'] = is_front_page();
		$context['get_url'] = $_SERVER['REQUEST_URI'];

		return $context;
	}

	// Menus / Theme Support / ACF Options Page
	function after_setup_theme() {
		register_nav_menu( 'primary', 'Main Navigation' );

		add_theme_support( 'menus' );
		add_theme_support( 'align-wide' );
		add_theme_support( 'post-thumbnails' );

		if( function_exists('acf_add_options_page') ) {
			acf_add_options_page([
				'page_title' => 'Footer Settings',
				'menu_title' => 'Footer Setup',
				'capability' => 'edit_posts',
				'redirect' => false,
				'updated_message' => 'Site Footer Updated!'
			]);
		}
	}

	// registers and renders our custom acf blocks
	function render_custom_acf_blocks() {
		require 'acf-block-functions.php';
	}

	// creates a custom category for our theme-specific blocks
	function mawp_block_category( $categories, $post ) {
		return array_merge( $categories,
			array( array(
				'slug' => 'mawp-blocks',
				'title' => 'MAWP Blocks'
			),
		));
	}

	// set what blocks are available to the block editor (admins get all blocks)
	function custom_block_picker( $allowed_blocks ) {
		if( ! current_user_can('manage_options') ) {
			$allowed_blocks = array(
				// Built-in blocks
				'core/image',
				'core/heading',
				'core/paragraph',
				'core/embed',
				'core-embed/facebook',
				'core-embed/youtube',
				'core-embed/twitter',

				// Custom Blocks
				'acf/logo-carousel',
				'acf/employee-block',
				'acf/button-block',
				'acf/arrow-button-block',
				'acf/employee-custom-block',
				'acf/dropdown-block',
			);

			return $allowed_blocks;
		}
	}

	// get rid of clutter
	function disable_admin_columns( $columns ) {
		unset( $columns['comments'] );
		return $columns;
	}

	// add cpts here
	function register_post_types() {
		include_once('custom-post-types/post-type-project.php');
		include_once('custom-post-types/post-type-employee.php');
		include_once('custom-post-types/post-type-capability.php');
	}

	// project taxonomy
	function project_taxonomy() {
		$labels = array(
			'name' 				=> _x( 'Project Types', 'mawp' ),
			'singular_name' 	=> _x( 'Project Type', 'mawp' ),
			'search_items' 		=> __( 'Search Project Types', 'mawp' ),
			'all_items' 		=> __( 'All Project Types', 'mawp' ),
			'edit_item' 		=> __( 'Edit Project Type', 'mawp' ),
			'update_item' 		=> __( 'Update Project Type', 'mawp' ),
			'add_new_item' 		=> __( 'Add New Project Type', 'mawp' ),
			'new_item_name' 	=> __( 'New Project Type', 'mawp' ),
			'menu_name' 		=> __( 'Project Types', 'mawp' ),
			'parent_item'		=> NULL, // remove parent items to prevent breaking if someone adds one
		);

		$args = array(
			'hierarchical' 	    => true,
			'labels' 	    	=> $labels,
			'show_ui' 	    	=> true,
			'show_admin_column' => true,
			'has_archive'		=> false,
			'query_var'	    	=> true,
			'show_in_rest'		=> true, // required to show categories in Block Editor sidebar
			'rewrite'			=> true,
		);
		register_taxonomy( 'project-type', 'project', $args );
	}
} // End of MAWPSite class

new MAWPSite();

// main site nav
function mawp_render_primary_menu() {
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container' => false,
		'menu_id' => 'primary-menu'
	));
}

// run if  _wp_page_template  is not empty (custom template is used)
// for the Default Template it will be empty. (default is used when no template is set)
// Code Courtesy of: Bill Erickson
function ea_disable_editor( $id = false ) {
	$excluded_templates = array(
		'front-page.php',
		'archive-project.php',
		'contact.php'
	);

	if( empty( $id ) )
		return false;

	$id = intval( $id );
	$template = get_page_template_slug( $id );

	return in_array( $template, $excluded_templates );
}

function ea_disable_gutenberg( $can_edit, $post_type ) {
	if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
		return $can_edit;

	if( ea_disable_editor( $_GET['post'] ) )
		$can_edit = false;

	return $can_edit;
}
add_filter( 'gutenberg_can_edit_post_type', 'ea_disable_gutenberg', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'ea_disable_gutenberg', 10, 2 );