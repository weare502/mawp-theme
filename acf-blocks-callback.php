<?php
/**
*  This is the callback that displays the blocks.
*  !IMPORTANT! - get_fields() is ambiguous - all custom fields must have a unique name
*
* @param   array $block - The block settings and attributes.
* @param   string $content - The block content (empty string).
* @param   bool $is_preview - True during AJAX preview.
*/

function acf_custom_blocks_callback( $block, $content = '', $is_preview = false ) {
	$context = Timber::get_context();

	$context['block'] = $block;
	$context['fields'] = get_fields();
	$context['is_preview'] = $is_preview;
	
	// for employees (all) block
	$args = [
		'post_type' => 'employee',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC'
	];
	$context['employees'] = Timber::get_posts($args);

	// templates/blocks/acf/BLOCK.twig
	$template = 'templates/blocks/' . $block['name'] . '.twig';
	Timber::render( $template, $context );
}