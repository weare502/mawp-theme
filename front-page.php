<?php
/**
 * Template Name: Home
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['project_types'] = Timber::get_terms( ['taxonomies' => 'project-type'] );

$context['projects'] = Timber::get_posts([
	'post_type' => 'project',
	'posts_per_page' => -1,
	'orderby' => 'date',
	'order' => 'DESC'
]);

$templates = array( 'front-page.twig' );

Timber::render( $templates, $context );