<?php
/**
 * Template Name: Capability Archive
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['capabilities'] = Timber::get_posts([
	'post_type' => 'capability',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order' => 'ASC'
]);

$templates = array( 'archive-capability.twig' );

Timber::render( $templates, $context );