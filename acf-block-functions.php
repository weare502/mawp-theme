<?php
// This functions file is for all custom blocks added via ACF
// Reference: https://www.advancedcustomfields.com/resources/acf_register_block_type/

if( function_exists('acf_register_block_type') ) :
	include 'acf-blocks-callback.php'; // pass-off to let Timber render the blocks

	/** Blocks **/
	$logo_carousel = array(
		'name' => 'logo-carousel',
		'title' => __( 'Infinite Logo Carousel', 'mawp' ),
		'description' => __( 'Creates a logo carousel that continuously loops.', 'mawp' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mawp-blocks',
		'align' => 'wide',
		'icon' => 'slides',
		'mode' => 'auto',
		'multiple' => false, // only allow this once per project
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'logo', 'carousel', 'slider', 'slide' )
	);
	acf_register_block_type( $logo_carousel );

	// attached to post type 'employee'
	$employee_block = array(
		'name' => 'employee-block',
		'title' => __( 'Employee Block (All)', 'mawp' ),
		'description' => __( 'Shows all employees alphabetically in a grid.', 'mawp' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mawp-blocks',
		'align' => 'center',
		'icon' => 'businessman',
		'mode' => 'auto',
		'multiple' => false, // only allow this once per project
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'employee', 'staff' )
	);
	acf_register_block_type( $employee_block );

	// attached to post type 'employee' - custom post object block
	$employee_custom_block = array(
		'name' => 'employee-custom-block',
		'title' => __( 'Employee Block (Custom)', 'mawp' ),
		'description' => __( 'User selectable employees.', 'mawp' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mawp-blocks',
		'icon' => 'admin-users',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'employee', 'staff', 'custom', 'select' )
	);
	acf_register_block_type( $employee_custom_block );

	$button_block = array(
		'name' => 'button-block',
		'title' => __( 'Standard Button', 'mawp' ),
		'description' => __( 'Standard button using the Theme\'s color palette.', 'mawp' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mawp-blocks',
		'icon' => 'editor-removeformatting',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'button', 'standard' )
	);
	acf_register_block_type( $button_block );

	$arrow_button_block = array(
		'name' => 'arrow-button-block',
		'title' => __( 'Arrow Button', 'mawp' ),
		'description' => __( 'Standard button with a right arrow using the Theme\'s color palette.', 'mawp' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mawp-blocks',
		'icon' => 'editor-removeformatting',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'button', 'arrow' )
	);
	acf_register_block_type( $arrow_button_block );

	$dropdown_block = array(
		'name' => 'dropdown-block',
		'title' => __( 'Dropdown Block', 'mawp' ),
		'description' => __( 'Simple dropdown / accordion. Content folds into the dropdown title and expands when clicked.', 'mawp' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mawp-blocks',
		'icon' => 'sort',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'drop', 'down', 'dropdown', 'accordion' )
	);
	acf_register_block_type( $dropdown_block );
endif;