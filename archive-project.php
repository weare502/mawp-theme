<?php
/**
 * Template Name: Project Archive
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['project_types'] = Timber::get_terms( ['taxonomies' => 'project-type'] );

$context['projects'] = Timber::get_posts([
	'post_type' => 'project',
	'posts_per_page' => -1,
	'orderby' => 'date',
	'order' => 'DESC'
]);

$templates = array( 'archive-project.twig' );

Timber::render( $templates, $context );